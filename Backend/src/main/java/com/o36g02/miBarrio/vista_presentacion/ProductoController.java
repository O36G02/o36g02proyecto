package com.o36g02.miBarrio.vista_presentacion;

import com.o36g02.miBarrio.controlador_persistencia.Producto;
import com.o36g02.miBarrio.modelo_logica.repository_service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class    ProductoController {

    // Atributos
    @Autowired  // logra conectar y utilizar el repositorio desde la clase.
    private ProductoService productoService;

    // Métodos o Funciones

    //Crear productos
    @PutMapping(value = "/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto){
        Producto obj = productoService.save(producto);
        return new ResponseEntity<Producto>(obj, HttpStatus.OK);
    }

    // Leer productos
    @GetMapping("/list")
    public List<Producto> consultarTodo(){
        return productoService.findAll();
    }

    //Leer productos por id
    @GetMapping("/list/{id}")
    public Producto consultaPorId(@PathVariable("id") Integer idProd){
        return productoService.findById(idProd);
    }

    // Actualizar los productos
    @PostMapping(value = "/")
    public ResponseEntity<Producto> editar(@RequestBody Producto producto){
        Producto obj = productoService.findById(producto.getIdProd());
        if (obj != null){
            obj.setNombreProd(producto.getNombreProd());
            obj.setPreciocompra_prod(producto.getPreciocompra_prod());
            obj.setPrecioventa_prod(producto.getPrecioventa_prod());
            obj.setCantidad_prod(producto.getCantidad_prod());
            productoService.save(obj);
            return new ResponseEntity<Producto>(obj, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Producto>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Eliminar los productos
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable("id") Integer idProd){
        Producto obj = productoService.findById(idProd);
        if (obj != null){
            productoService.delete(idProd);
            return new ResponseEntity<Producto>(obj, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Producto>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
