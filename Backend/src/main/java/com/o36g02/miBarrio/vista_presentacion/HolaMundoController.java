package com.o36g02.miBarrio.vista_presentacion;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Agragamos las siguientes anotaciones
@RestController
@RequestMapping("/compras")  // responde bajo la ruta saludar
public class HolaMundoController {

    // Creamos un nuevo controlador
    @GetMapping("/productos") // el método responde bajo la anotación GetMapping
    public String saludar(){

        return "<br> <h1>Tienda de electrosuministros</h1>... <h3>Bienvenidos a la tienda de suministros de telecomunicaciones</h3>";

    }
}
