package com.o36g02.miBarrio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O36g02miBarrioApplication {

	public static void main(String[] args) {
		SpringApplication.run(O36g02miBarrioApplication.class, args);
	}

}
