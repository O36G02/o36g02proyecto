package com.o36g02.miBarrio.controlador_persistencia;

import javax.persistence.*;

// La Anotación Más Importante
@Entity  // Hará entender a Java que es una clase que mapea una entidad de la base de datos
@Table(name = "producto")  // Debido a que el nombre de la tabla es diferente del nombre de la clase.
public class Producto {
    // Atributos referentes a la tabla o entidad
    @Id  // Anotación por ser llave primaria
    // @GeneratedValue(strategy = GenerationType.IDENTITY)  // Utilizar en caso que en la base de datos el PK sea autoincremental.
    @Column(name = "id_prod")   // Teniendo en cuenta que el nombre del campo en la tabla es diferente al nombre del atributo
    private Integer idProd;

    @Column(name = "nombre_prod")
    private String nombreProd;

    private Float preciocompra_prod;
    private Float precioventa_prod;
    private Integer cantidad_prod;


    // Métodos - Funciones


    public Integer getIdProd() {
        return idProd;
    }

    public void setIdProd(Integer idProd) {
        this.idProd = idProd;
    }

    public String getNombreProd() {
        return nombreProd;
    }

    public void setNombreProd(String nombreProd) {
        this.nombreProd = nombreProd;
    }

    public Float getPreciocompra_prod() {
        return preciocompra_prod;
    }

    public void setPreciocompra_prod(Float preciocompra_prod) {
        this.preciocompra_prod = preciocompra_prod;
    }

    public Float getPrecioventa_prod() {
        return precioventa_prod;
    }

    public void setPrecioventa_prod(Float precioventa_prod) {
        this.precioventa_prod = precioventa_prod;
    }

    public Integer getCantidad_prod() {
        return cantidad_prod;
    }

    public void setCantidad_prod(Integer cantidad_prod) {
        this.cantidad_prod = cantidad_prod;
    }
}
