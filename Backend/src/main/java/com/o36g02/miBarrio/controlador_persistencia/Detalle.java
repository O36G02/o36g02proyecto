package com.o36g02.miBarrio.controlador_persistencia;

import javax.persistence.*;

@Entity
@Table(name = "detalle")
public class Detalle {

    // Atributos
    @Id
    private Integer id_det;

    @ManyToOne  // Revisando el diagrama de entidad realición.
    @JoinColumn(name = "id_prod", insertable = false, updatable = false)
    private Producto id_prod;
    //private Integer id_prod;

    @ManyToOne  // Revisando el diagrama de entidad realición.
    @JoinColumn(name = "id_trans", insertable = false, updatable = false)
    private Transaccion id_trans;
    //private Integer id_trans;

    private Float valorDetalle_det;
    private Integer cantidadDetalle_det;
    private Float totalDetalle_det;

    // Métodos - Funciones

    public Integer getId_det() {
        return id_det;
    }

    public void setId_det(Integer id_det) {
        this.id_det = id_det;
    }

    public Producto getId_prod() {
        return id_prod;
    }

    public void setId_prod(Producto id_prod) {
        this.id_prod = id_prod;
    }

    public Transaccion getId_trans() {
        return id_trans;
    }

    public void setId_trans(Transaccion id_trans) {
        this.id_trans = id_trans;
    }

    public Float getValorDetalle_det() {
        return valorDetalle_det;
    }

    public void setValorDetalle_det(Float valorDetalle_det) {
        this.valorDetalle_det = valorDetalle_det;
    }

    public Integer getCantidadDetalle_det() {
        return cantidadDetalle_det;
    }

    public void setCantidadDetalle_det(Integer cantidadDetalle_det) {
        this.cantidadDetalle_det = cantidadDetalle_det;
    }

    public Float getTotalDetalle_det() {
        return totalDetalle_det;
    }

    public void setTotalDetalle_det(Float totalDetalle_det) {
        this.totalDetalle_det = totalDetalle_det;
    }
}
