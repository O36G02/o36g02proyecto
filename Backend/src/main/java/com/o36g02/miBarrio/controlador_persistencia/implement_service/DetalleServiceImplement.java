package com.o36g02.miBarrio.controlador_persistencia.implement_service;

import com.o36g02.miBarrio.controlador_persistencia.Detalle;
import com.o36g02.miBarrio.modelo_logica.DetalleCrudRepositoryDao;
import com.o36g02.miBarrio.modelo_logica.repository_service.DetalleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DetalleServiceImplement implements DetalleService {

    // Atributos
    @Autowired // permite conectar y utilizar el repositorio desde la clase.
    private DetalleCrudRepositoryDao detalleCrudRepositoryDao;

    // Métodos - Funciones
    @Override
    @Transactional  // establece si el método puede afecta o no la integridad de los datos
    public Detalle save(Detalle detalle){
        return detalleCrudRepositoryDao.save(detalle);
    }

    @Override
    @Transactional  // establece si el método puede afecta o no la integridad de los datos
    public void delete(Integer idDet){
        detalleCrudRepositoryDao.deleteById(idDet);
    }

    @Override
    @Transactional  // establece si el método puede afecta o no la integridad de los datos
    public Detalle findById(Integer idDet){
        return detalleCrudRepositoryDao.findById(idDet).orElse(null);
    }

    @Override
    @Transactional  // establece si el método puede afecta o no la integridad de los datos
    public List<Detalle> findAll(){
        return (List<Detalle>) detalleCrudRepositoryDao.findAll();
    }
}
