package com.o36g02.miBarrio.controlador_persistencia.implement_service;

import com.o36g02.miBarrio.controlador_persistencia.Transaccion;
import com.o36g02.miBarrio.modelo_logica.TransaccionCrudRepositoryDao;
import com.o36g02.miBarrio.modelo_logica.repository_service.TransaccionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TransaccionServiceImplement implements TransaccionService {

    // Atributos
    @Autowired // permite conectar y utilizar el repositorio desde la clase.
    private TransaccionCrudRepositoryDao transaccionCrudRepositoryDao;

    // Métodos - Funciones
    @Override
    @Transactional  // establece si el método puede afecta o no la integridad de los datos
    public Transaccion save(Transaccion transaccion){
        return transaccionCrudRepositoryDao.save(transaccion);
    }

    @Override
    @Transactional  // establece si el método puede afecta o no la integridad de los datos
    public void delete(Integer idTrans){
        transaccionCrudRepositoryDao.deleteById(idTrans);
    }

    @Override
    @Transactional  // establece si el método puede afecta o no la integridad de los datos
    public Transaccion findById(Integer idTrans){
        return transaccionCrudRepositoryDao.findById(idTrans).orElse(null);
    }

    @Override
    @Transactional  // establece si el método puede afecta o no la integridad de los datos
    public List<Transaccion> findAll(){
        return (List<Transaccion>) transaccionCrudRepositoryDao.findAll();
    }
}
