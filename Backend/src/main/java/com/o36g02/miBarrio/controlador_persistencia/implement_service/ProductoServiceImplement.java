package com.o36g02.miBarrio.controlador_persistencia.implement_service;

import com.o36g02.miBarrio.controlador_persistencia.Producto;
import com.o36g02.miBarrio.modelo_logica.ProductoCrudRepositoryDao;
import com.o36g02.miBarrio.modelo_logica.repository_service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductoServiceImplement implements ProductoService {

    // Atributo
    @Autowired // permite conectar y utilizar el repositorio desde la clase.
    private ProductoCrudRepositoryDao productoCrudRepositoryDao;

    // Métodos - Funciones
    @Override
    @Transactional(readOnly = false)  // establece si el método puede afecta o no la integridad de los datos
    public Producto save(Producto producto){
        return productoCrudRepositoryDao.save(producto);
    }

    @Override
    @Transactional(readOnly = false)  // establece si el método puede afecta o no la integridad de los datos
    public void delete(Integer idProd){
        productoCrudRepositoryDao.deleteById(idProd);
    }

    @Override
    @Transactional(readOnly = true)  // establece si el método puede afecta o no la integridad de los datos
    public Producto findById(Integer idProd){
        return productoCrudRepositoryDao.findById(idProd).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)  // establece si el método puede afecta o no la integridad de los datos
    public List<Producto> findAll(){
        return (List<Producto>) productoCrudRepositoryDao.findAll();
    }
}
