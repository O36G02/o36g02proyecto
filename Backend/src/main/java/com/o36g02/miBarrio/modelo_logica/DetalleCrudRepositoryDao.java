package com.o36g02.miBarrio.modelo_logica;

import com.o36g02.miBarrio.controlador_persistencia.Detalle;
import org.springframework.data.repository.CrudRepository;

public interface DetalleCrudRepositoryDao extends CrudRepository<Detalle, Integer> {
}
