package com.o36g02.miBarrio.modelo_logica.repository_service;

import com.o36g02.miBarrio.controlador_persistencia.Detalle;

import java.util.List;

public interface DetalleService {

    public Detalle save(Detalle detalle);
    public void delete(Integer idDet);
    public Detalle findById(Integer idDet);
    public List<Detalle> findAll();
}
