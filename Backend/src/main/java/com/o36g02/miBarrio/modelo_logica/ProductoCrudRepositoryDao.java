package com.o36g02.miBarrio.modelo_logica;

import com.o36g02.miBarrio.controlador_persistencia.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoCrudRepositoryDao extends CrudRepository<Producto, Integer> {

    // Forma tradicional
    // @Query(value = "SELECT * FROM producto", nativeQuery = true)

    // Nueva Forma
    // List<Producto> findAll();
}
