package com.o36g02.miBarrio.modelo_logica;

import com.o36g02.miBarrio.controlador_persistencia.Transaccion;
import org.springframework.data.repository.CrudRepository;

public interface TransaccionCrudRepositoryDao extends CrudRepository<Transaccion, Integer> {
}
